import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpResponse, HttpRequest, HttpHandler, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
import { take, map, exhaustMap, tap, retry, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {


    constructor(
        private authService: AuthenticationService,
        private router: Router
    ) {


    }

    intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<any> {

        return this.authService.userAccessToken.pipe(take(1), exhaustMap(data => {

            if (!data)
                return next.handle(httpRequest)

            const modifiedRequest = httpRequest.clone({
                setHeaders: {
                    'Authorization': `Bearer ${data}`
                },

            });
            return next.handle(modifiedRequest)
                .pipe(
                    retry(1),
                    catchError((error: HttpErrorResponse) => {
                        let errorMessage = '';
                        if (error.error instanceof ErrorEvent) {
                            // client-side error
                            errorMessage = `Error: ${error.error.message}`;
                        } else {
                            // server-side error
                            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
                        }
                        window.alert(errorMessage);
                        if (error.status == 401) {
                            localStorage.removeItem('jwt');
                            this.authService.userAccessToken.next(null)
                            this.router.navigate(["auth"]);

                        }
                        return throwError(errorMessage);
                    })
                )
        }))

    }


}