export interface User{
    id:string;
    emailId:string;
    password:string;
    userName:string;
    accessToken?:string;
}

export interface LoginResponse{
    error:boolean;
    data:{
        jwt?:string;
        responseMsg?:any;
        user?:string
        userDetails?:User;
    }
}

export interface Response{
    error:boolean;
    data:{
        jwt?:string;
        responseMsg?:any;
        user?:string
        userDetails?:User;
    }
}
export interface Project{
    projectId:string;
    eOfficeProjectId:string;
    description:string;
    name:string;
    creationDate: any;
    startDate?:any;
    estimatedEndDate?:any;
    projectEstimate?:number;
    assignedTo:string;
}

export interface Transaction{
    projectId: string;
    fromUserId:number;
    toUserId:number,
    description?: string,
    remarks?:string,
    startDate?:string,
    endDate?:any,
    status?:string;
    noOfDaysToCompleteTheTask:number
}


export interface TransactionWithUserDetails extends Transaction{
    fromUserDetails:User;
    toUserDetails:User;
}