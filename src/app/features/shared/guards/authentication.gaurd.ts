import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import {take, map} from "rxjs/operators"
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGaurd implements CanActivate{

  constructor(
    private authService:AuthenticationService,
    private router:Router
    ) { }

  canActivate():boolean | UrlTree | Observable<boolean | UrlTree>
  {
    let data = this.authService.userAccessToken.value;
        if(!data)
        {
          console.log("redirecting to auth")
          return this.router.createUrlTree(["auth"])
        }
        else{
          return true
        }
      
  }
}
