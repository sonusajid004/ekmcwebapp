import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User, LoginResponse } from '../types';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  userAccessToken: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  constructor(
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService
  ) { }


  login(data: User) {

    this.http.post(environment.baseURL + "login", data, {
      observe: "body",
    })
      .subscribe((res: LoginResponse) => {
        console.log(res)
        if (res.error) {
          this.toastr.error(res.data.responseMsg);
          return;
        }


        localStorage.setItem("jwt", res.data.jwt)
        localStorage.setItem('emailId',data.emailId);
        localStorage.setItem("userDetails",JSON.stringify(res.data.userDetails));
        this.userAccessToken.next(res.data.jwt);

        if (!res.error) {
          this.router.navigate(["home"]);
          this.toastr.success("LoggedIn Successfully!");
        }
      }, (err) => {
        console.log(err.message)

      })
  }

  register(data: User) {
    this.http.post(environment.baseURL + "register", data, {
      observe: "body"
    })
      .subscribe((res: LoginResponse) => {
        console.log(res);
        this.toastr.error(res.data.responseMsg);
        if (!res.error) {
          this.router.navigate(["home"]);
          this.toastr.success("Registered Successfully!");
        }

      }, (err) => {
        alert(err.message)
      })
  }

}
