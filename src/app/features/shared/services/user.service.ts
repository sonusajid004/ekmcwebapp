import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService
  ) { }

  getLoggedInUserEmailId()
  {
    return localStorage.getItem('emailId');
  }

  getCurrentUser()
  {
    let data = {
      emailId:this.getLoggedInUserEmailId()
    }
    console.log(data);
    return this.http.post(environment.baseURL + "getUser", data, {
      observe: "body",
    })
    
  }


  getUserIds()
  {
    return this.http.get(environment.baseURL + "getUserIds",{
      observe:"body"
    })
  }
}
