import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Project, Response } from '../types';
import * as moment from "moment";
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';
import { takeLast, tap, take } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ProjectService {


  selectedProjectDetails = new Subject<Project>(); 
  constructor(
    private http: HttpClient,
    private router: Router,
    private toastr: ToastrService
  ) { }

  addProject(data:Project)
  {
    
    return this.http.post(environment.baseURL + "createProject", data, {
      observe: "body",
    })

  }
  convertStringToDate(str:string)
  {
    return moment(str,"YYYY-MM-DD").toDate().toISOString().slice(0, 19).replace('T', ' ');

  }

  getAllProjects()
  {
    return this.http.get(environment.baseURL+"getAllProjects",{
      observe:"body",
    })
  }


  getProjectIds(userId:number)
  {
      return this.http.get(environment.baseURL+"getProjectIds",{
        params:{
          userId:userId.toString()
        },
        observe:"body"
      })
  }

  getProjectDetails(projectId:number)
  {
   
    return this.http.get(environment.baseURL+"getProjectDetails",{
      params:{
        projectId:projectId.toString()
      },
      observe:"body"
    })
  }

}
