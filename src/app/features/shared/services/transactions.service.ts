import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Transaction } from '../types';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  constructor(
    private http:HttpClient,
    
  ) { }

  createTransaction(data:Transaction)
  {
    return this.http.post(environment.baseURL+"createTransaction",data,{
      observe:"body"
    })
  }

  getTransactionsForSpecificProject(projectId:number)
  {
    return this.http.get(environment.baseURL + "getTransactionsForSpecificProject",{
      observe:"body",
      params:{
        projectId:projectId.toString()
      }
    })
  }
}
