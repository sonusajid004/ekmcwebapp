import { Routes, RouterModule } from "@angular/router";
import { ProjectsComponent } from './projects.component';
import { AllProjectsComponent } from './all-projects/all-projects.component';
import { AddProjectComponent } from './add-project/add-project.component';
import { NgModule } from '@angular/core';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { ProjectUpdateComponent } from './project-update/project-update.component';

const routes: Routes = [
    {
        path:'',
        component:ProjectsComponent,
        children:[
            {
                path:'',
                component:AllProjectsComponent
            },
            {
                path:'addProject',
                component:AddProjectComponent
            },
            {
                path:'projectDetails',
                component:ProjectDetailsComponent
            },
            {
                path:'projectUpdate',
                component:ProjectUpdateComponent
            }
        ]
    },
     
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class ProjectsRoutingModule{}