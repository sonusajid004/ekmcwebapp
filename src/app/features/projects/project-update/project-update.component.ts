import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProjectService } from '../../shared/services/project.service';
import { takeLast } from 'rxjs/operators';
import { Response, Project, User, Transaction } from '../../shared/types';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { UserService } from '../../shared/services/user.service';
import * as  moment from "moment";
import { TransactionsService } from '../../shared/services/transactions.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Component({
  selector: 'app-project-update',
  templateUrl: './project-update.component.html',
  styleUrls: ['./project-update.component.scss']
})
export class ProjectUpdateComponent implements OnInit, OnDestroy {

  projectIds: { projectId: number }[];
  updateForm: FormGroup;
  projectIdValueSub: Subscription;
  selectedProject: Project;
  otherUsers:{id:number,userName:string,emailId:string}[]=[];
  currentUser:User;
  today:string;
  endMinLimit:string;
  statusPossibilities:string[];
  projectStartDateSub:Subscription;

  constructor(
    private projService: ProjectService,
    private userService:UserService,
    private transacService:TransactionsService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.statusPossibilities = ["Initiated", "Inprogress","Pending","Running"];
    this.today = moment(new Date()).format("YYYY-MM-DD");
    this.endMinLimit = this.today;
    let user = JSON.parse(localStorage.getItem("userDetails")) as User;
    this.currentUser = user;
    // this.selectedProjectDetailsSubscription = this.projService.selectedProjectDetails.subscribe((data)=>{
    //   console.log("Project Selected",data);
    // })
    this.updateForm = this.fb.group({
      projectId: new FormControl(null),
      fromUserId:new FormControl(user.id,[Validators.required]),
      toUserId:new FormControl(null),
      description: new FormControl(null),
      remarks:new FormControl(null),
      startDate:new FormControl(null),
      endDate:new FormControl(null),
      status:new FormControl(null),
      noOfDaysToCompleteTheTask:new FormControl(null,Validators.required)
    })
    this.projService.getProjectIds(parseInt(user.id)).pipe((takeLast(1)))
      .subscribe((data: Response) => {
        if(!data.error)
        this.projectIds = data.data.responseMsg;
      })

    this.userService.getUserIds().pipe(takeLast(1))
      .subscribe((data:Response)=>{
        if(!data.error)
        {
          this.otherUsers = data.data.responseMsg.filter(item=>item.id!=user.id);
        }
       
      })
    this.projectIdValueSub = this.updateForm.get('projectId').valueChanges
      .subscribe((data) => {
        if(data)
        this.projService.getProjectDetails(data)
          .pipe(takeLast(1))
          .subscribe((data: Response) => {
            if (!data.error) {

              this.selectedProject = data.data.responseMsg[0];
              console.log(this.selectedProject, "selected")
            }
          })
      })
      this.projectStartDateSub = this.updateForm.get('startDate').valueChanges.subscribe((data)=>{
       if(data)
        this.endMinLimit = data;
       
      })
  }

  goBack() {
    this.router.navigate(["projects"])
  }

  resetForm()
  {
    this.updateForm.reset();
  }

  submitForm()
  {
    if(!this.updateForm.valid)
    {
      return;
    }
    
    let data = this.updateForm.value as Transaction;
    
   
    if(data.startDate)
    data.startDate = this.projService.convertStringToDate(data.startDate);
    if(data.endDate)
    data.endDate = this.projService.convertStringToDate(data.endDate);
    
    this.transacService.createTransaction(data)
      .pipe(takeLast(1))
      .subscribe((data:Response)=>{
        
        if(!data.error)
        {
          this.toastr.success("Inserted data");
          this.updateForm.reset();
        }
        else {
          this.toastr.error(data.data.responseMsg);
        }
      })
  }

  ngOnDestroy() {
    if (this.projectIdValueSub)
      this.projectIdValueSub.unsubscribe();
    if(this.projectStartDateSub)
    this.projectStartDateSub.unsubscribe();

  }

}
