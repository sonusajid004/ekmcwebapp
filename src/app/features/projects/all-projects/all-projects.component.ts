import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectService } from '../../shared/services/project.service';
import { takeLast } from 'rxjs/operators';
import { Response, Project, User } from '../../shared/types';

@Component({
  selector: 'app-all-projects',
  templateUrl: './all-projects.component.html',
  styleUrls: ['./all-projects.component.scss']
})
export class AllProjectsComponent implements OnInit {

  loading:boolean;
  projects:(Project|User)[];
  constructor(
    private router:Router,
    private projService:ProjectService
  ) {
    this.loading = true;
    this.projects = [];
   }

  ngOnInit(): void {
    this.projService.getAllProjects().pipe(takeLast(1))
      .subscribe((data:Response)=>{
        if(!data.error)
        {
          // console.log(data.data.responseMsg)
          this.loading = false
          this.projects = data.data.responseMsg as any[];
        }
        else
        {
          console.log(data.data.responseMsg)
        }
      })
        
  }
  
  routeToProjectDetails(item:Project)
  {
    this.router.navigate(["projects/projectDetails"],{
      queryParams:{
        projectId:item.projectId
      }
    })
  }
  routeToAddProject()
  {
    this.router.navigate(["projects/addProject"])

  }
  routeToUpdateProject()
  {
    this.router.navigate(["projects/projectUpdate"])
  }

  

}
