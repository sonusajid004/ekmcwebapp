import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../../shared/services/user.service';
import { takeLast } from 'rxjs/operators';
import { User, Project,Response } from '../../shared/types';
import { Router } from '@angular/router';
import * as moment from "moment";
import { ProjectService } from '../../shared/services/project.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.scss']
})
export class AddProjectComponent implements OnInit {

  projectForm: FormGroup;
  currentUser: User;
  loading: boolean;
  today:string;
  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router,
    private projectService: ProjectService,
    private toastr: ToastrService
  ) {
    this.loading = true;
  }

  ngOnInit(): void {
    
    this.today = moment(new Date()).format("YYYY-MM-DD");
    this.userService.getCurrentUser().pipe(takeLast(1)).subscribe((res: any) => {
      this.currentUser = res.data.user;
      this.loading = false;
      this.projectForm.patchValue({
        assignedTo:this.currentUser.id
      })
    })

    this.projectForm = this.fb.group({
      projectId: new FormControl(null, [Validators.required]),
      eOfficeProjectId: new FormControl(null, [Validators.required]),
      description: new FormControl(null, [Validators.required]),
      name: new FormControl(null, [Validators.required]),
      creationDate: new FormControl(moment(new Date()).format("YYYY-MM-DD"), [Validators.required]),
      startDate: new FormControl(null, []),
      estimatedEndDate: new FormControl(null, []),
      projectEstimate: new FormControl(null, []),
      assignedTo: new FormControl(null, [Validators.required])
    })

  }

  goBack() {
    this.router.navigate(["projects"])
  }
  submitForm() {
    if (!this.projectForm.valid)
      return;

    let data: Project = this.projectForm.value;
    data.creationDate = this.projectService.convertStringToDate(data.creationDate);
    if (data.startDate)
      data.startDate = this.projectService.convertStringToDate(data.startDate);
    if (data.estimatedEndDate)
      data.estimatedEndDate = this.projectService.convertStringToDate(data.estimatedEndDate);
  
    this.projectService.addProject(data)
      .pipe(takeLast(1))
      .subscribe((data:Response)=>{
       
        if(!data.error)
        {
          this.toastr.success("Inserted data");
          this.projectForm.reset();
        }
        else {
          this.toastr.error(data.data.responseMsg);
        }
        
        
      },(err)=>{

      })
  }

  resetForm() {
    this.projectForm.reset();
  }

}
