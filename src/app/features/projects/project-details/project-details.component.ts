import { Component, OnInit } from '@angular/core';
import { TransactionsService } from '../../shared/services/transactions.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { takeLast } from 'rxjs/operators';
import { TransactionWithUserDetails, Response } from '../../shared/types';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit {

  routeSub:Subscription;
  transactionDetails:TransactionWithUserDetails[];
  loading:boolean;
  constructor(
    private transacService:TransactionsService,
    private route:ActivatedRoute,
    private toastr: ToastrService,
    private router:Router
  ) { 
    this.loading = true;
  }

  ngOnInit(): void {
    this.routeSub = this.route.queryParams.subscribe((data)=>{
      if(data?.projectId)
      this.transacService.getTransactionsForSpecificProject(data.projectId)
          .pipe(takeLast(1)).subscribe((data:Response)=>{
            console.log(data);

            if(!data.error)
            {
              this.loading = false;

              this.transactionDetails = data.data.responseMsg;
            }
            else{
                this.toastr.error("Error in retrieving data!");
            }
           
          })
    })
  }

  goBack() {
    this.router.navigate(["projects"])
  }
}
