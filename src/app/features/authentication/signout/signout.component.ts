import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../shared/services/authentication.service';

@Component({
  selector: 'app-signout',
  templateUrl: './signout.component.html',
  styleUrls: ['./signout.component.scss']
})
export class SignoutComponent implements OnInit {

  constructor(
    private router:Router,
    private authService:AuthenticationService
  ) { }

  ngOnInit(): void {
    let data = this.authService.userAccessToken.value;
    if(data)
    {
      this.authService.userAccessToken.next(null);
     localStorage.clear();
    }
    else
    this.router.navigate(["auth"]);
  }

  signOut()
  {
    
  }
}
