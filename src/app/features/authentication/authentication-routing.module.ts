import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { SignoutComponent } from './signout/signout.component';

const routes: Routes = [
     {
         path:"registration",
         component:RegistrationComponent
     },
     {
        path:"login",
        component:LoginComponent
     },
     {
      path:'signout',
      component:SignoutComponent
    },
     {
         path:"**",
         redirectTo:"login"
     }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
