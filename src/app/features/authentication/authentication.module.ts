import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { RegistrationComponent } from './registration/registration.component';
import {MatCardModule} from '@angular/material/card'; 
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { LoginComponent } from './login/login.component';
import {MatButtonModule} from "@angular/material/button";
import { SignoutComponent } from './signout/signout.component';

@NgModule({
  declarations: [RegistrationComponent, LoginComponent,SignoutComponent],
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    MatCardModule,
    ReactiveFormsModule,
    MatButtonModule,
  MatInputModule,
  MatFormFieldModule
  ]
})
export class AuthenticationModule { }
