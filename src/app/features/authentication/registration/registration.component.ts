import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ComparePassword, MyErrorStateMatcher } from '../../shared/custom-validators';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { Router } from '@angular/router';





@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  regForm:FormGroup;
  matcher:MyErrorStateMatcher;
  constructor(
    private fb: FormBuilder,
    private authService:AuthenticationService,
    private router:Router
    ) {
    this.matcher = new MyErrorStateMatcher();
   }


  ngOnInit(): void {
    let data = this.authService.userAccessToken.value;
      if(data)
      {
        this.router.navigate(["home"])
      }
    this.regForm = new FormGroup({
      'userName':new FormControl(null,[Validators.required]),
      'emailId':new FormControl(null,[Validators.required,Validators.email]),
      'password':new FormControl(null,[Validators.required,Validators.minLength(8)]),
      'confirmPassword':new FormControl(null,[Validators.required,Validators.minLength(8),this.confirmPwdValidator.bind(this)]),
    },{
      
       updateOn: 'blur'
    })
  }
  
  resetForm()
  {
    this.regForm.reset();
  }

  confirmPwdValidator(c:FormControl)
  {
    if(!this.regForm)
    return;
    let pwdValue = this.regForm.get('password').value;
    let confirmPwdValue =  this.regForm.get('confirmPassword').value;
    if(pwdValue!=confirmPwdValue)
    return {
      'password mismatch':"Passwords must match"
    }
    else return null
  }


  onRegister()
  {
    
    if(!this.regForm.valid)
    {
      return;
    }
    this.authService.register(this.regForm.value);
  }

  routeToLogin()
  {
      this.router.navigate(["auth/login"])
  }

}
