import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup;
  constructor(
    private fb:FormBuilder,
    private authService:AuthenticationService,
    private router:Router
    ) { 
      let data = this.authService.userAccessToken.value;
      if(data)
      {
        this.router.navigate(["home"])
      }
     
    
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      'emailId':new FormControl(null,[Validators.required]),
      'password':new FormControl(null,[Validators.required])
    })
  }

  onLogin(){
    if(!this.loginForm.valid)
    return;
    try{
      this.authService.login(this.loginForm.value)
    }
    catch(err)
    {
      alert(err.message);
    }
    
  }
  routeToRegister()
  {
    this.router.navigate(["auth/registration"])
  }

  resetForm()
  {
    this.loginForm.reset();
  }

}
