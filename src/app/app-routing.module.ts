import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './features/core/layout/layout.component';
import { AuthenticationGaurd } from './features/shared/guards/authentication.gaurd';

const routes: Routes = [
  
    {
      path: 'auth',
      loadChildren: () => import('./features/authentication/authentication.module').then(m => m.AuthenticationModule),
    },
    {
      path:'',
      component:LayoutComponent,
      canActivate:[AuthenticationGaurd],
      children:[
        
        {
          path:'home',
          loadChildren:() => import('./features/home/home.module').then(m => m.HomeModule),
        },
        {
          path:'projects',
          loadChildren:() => import('./features/projects/projects.module').then(m => m.ProjectsModule),
        },
      ]
    },
    {
      path:"**",
      redirectTo:'home'
    }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
