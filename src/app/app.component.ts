import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './features/shared/services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'EKMCWebapp';

  constructor(private authService:AuthenticationService)
  {
  
  }
  ngOnInit()
  {
    let x = localStorage.getItem("jwt");
    console.log(x);
    if(x!=null)
    {
      this.authService.userAccessToken.next(x);
    }

  }
}
